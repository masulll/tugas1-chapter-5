const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
const readline = require("readline"); //membentuk input
// const data = {
//   id: uuidv4(),
//   name: "Sultan",
//   address: "Earth",
// };

// const datajson = require("./data.json");
// console.log(datajson);
// write file
// fs.writeFileSync("data.json", JSON.stringify(data));

// fs.writeFileSync("data.txt", JSON.stringify(data));
// readfileasync
// const datatxt = fs.readFile("./data.txt", "utf-8", (err, data) => {
//   if (err) throw error;
//   console.log(data);
// });

// readfileSync
// const datatxt = fs.readFileSync("./data.txt", "utf-8");
// const data = [
//   {
//     id: uuidv4(),
//     name: "Sultan",
//     address: "Earth",
//   },
// ];

// mengubah dari string ke JSON
// console.log(JSON.parse(datatxt));

class FileManager {
  constructor(file) {
    this.filename = filename;
    this.data = this.#readFile(filename);
  }
  //   membaca datatxt
  #readFile(path) {
    const datatxt = fs.readFileSync(path, "utf-8");
    return JSON.parse(datatxt);
  }
  // create datatxt secara async
  createData(newData) {
    //menambahkan item objek dari data yang sudah ada
    let finaldata = this.data;
    finaldata.push(newData);
    const _datastr = JSON.stringify(finaldata);
    fs.writeFile(this.filename, _datastr, (e) => {
      console.log(e);
    });
    return true;
  }

  updatedata(id) {
    let _update = this.data;
    let idx = _update.findIndex((x) => x.id === id);

    // const rl = readline.createInterface({
    //   input: process.stdin,
    //   output: process.stdout,
    // });

    rl.question("Change name : (enter to skip)", (name) => {
      rl.question("address : (enter to skip) ", (address) => {
        const data = { name, address };
        _update[idx].name = data.name;
        _update[idx].address = data.address;

        fs.writeFileSync(this.filename, JSON.stringify(_update));
        console.log(`===UPDATED DATA ID : ${id} ====`);
        console.log(_update);
        rl.close();
      });
    });
  }

  deletedata(id) {
    //menghapus data berdasarkan ID pada array objek
    let _finaldata = this.data;
    let idx = _finaldata.findIndex((x) => x.id == id); //mencari index array
    console.log(idx, "<<IDx");
    _finaldata.splice(idx, 1);
    console.log(_finaldata);
    fs.writeFileSync(this.filename, JSON.stringify(_finaldata));

    console.log("delete data berhasil");
  }
}
const filename = "./data.txt";

const file = new FileManager(filename);

// console.log(file.data);

// let data = FileManager.readFile(filename);

// const newData = {
//   id: uuidv4(),
//   name: "ROY",
//   address: "Earth",
// };

//-- create --
// file.createData(newData);
// console.log(file.data);
// -- delete --
// file.deletedata("8a9d043-f11c-44bb-9e1b-b6abf9a83b89");

// --update--
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Masukkan ID : ", (ID) => {
  file.updatedata(ID);
});

// file.updatedata(`${ID}`);

// FileManager.createData(filename, data);
